with (import <nixpkgs> {});

python3Packages.buildPythonPackage {
  name = "pySatie";

  src = builtins.fetchGit {
    url = "https://gitlab.com/sat-metalab/PySATIE.git";
    ref = "master";
  };

  propagatedBuildInputs = with python3Packages; [
    cython
    pyliblo
  ];
}
