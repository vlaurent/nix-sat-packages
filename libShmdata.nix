with import <nixpkgs> {};

let
  debug = true;
  
  gstDeps = with gst_all_1; [
    gstreamer
    gst-plugins-base
  ];
  
  commonDeps = [
    pcre
    libunwind
    elfutils
    pkgconfig
    bison
    flex
    libtool
    python3
  ];
in stdenv.mkDerivation {
  name = "libShmdata";

  src = fetchGit {
    url = "https://gitlab.com/sat-metalab/shmdata.git";
    ref = "master";
  };
  
  # bind gstreamer-1.0 from nix gstreamer packages
  NIX_CFLAGS_COMPILE =
    let gstPluginPaths =
          lib.makeSearchPathOutput "lib" "/lib/gstreamer-1.0" gstDeps;
    in [
      "-I${gst_all_1.gstreamer.dev}/lib/gstreamer-1.0/include"
      ''-DGST_PLUGIN_PATH_1_0="${gstPluginPaths}"''
    ];

  buildInputs = gstDeps ++ commonDeps;

  nativeBuildInputs = [
    python3Packages.wrapPython
    cmake
  ];

  cmakeFlags = [
   "-DCMAKE_BUILD_TYPE=${if debug then "Debug" else "Release"}"
  ];

  prePatch = ''
    substituteInPlace wrappers/python/CMakeLists.txt \
      --replace 'DESTINATION ''${PYTHON_DIST_PACKAGES}' 'DESTINATION "${python3.sitePackages}"'
  '';
}
