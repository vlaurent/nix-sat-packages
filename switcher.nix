with import <nixpkgs> {};

let
  version = "1.0.0";
  debug = true;

  libShmdata = import ./libShmdata.nix;

  gstDeps = with gst_all_1; [
    gstreamer
    gst-plugins-base
    gst-plugins-good
    gst-plugins-ugly
    gst-plugins-bad
    gst-libav
  ];

  xorgDeps = with xorg; [
    libXrandr
    libXinerama
    libXcursor
    libXi
  ];

  commonDeps = [
    libcxx
    libusb
    elfutils
    libsepol
    libselinux
    libShmdata
    pcre
    libunwind
    orc
    bison
    flex
    libtool
    glib
    json-glib
    gsoap
    liblo
    glibc
    libpulseaudio
    portmidi
    libjack2
    jack2
    libvncserver
    libuuid
    openssl
    swh_lv2
    mesa_noglu
    libGL_driver
    freeglut
    libltc
    curl
    fomp
    libsamplerate
    libresample
    python3
    git
    gnome3.libsoup
  ];
in stdenv.mkDerivation rec {
  name = "switcher";

  src = fetchgit {
    url = "https://gitlab.com/sat-metalab/switcher.git";
    rev = "bec86a63761c98bcc7f04b9323c5bb0b240bc7ad";
    sha256 = "1b09yq7jsmrqpqx34k98pwxl0cp47csqcv2j3haab2h8gar0cjy1";
    fetchSubmodules = true;
  };

  # bind gstreamer-1.0 from nix gstreamer packages
  NIX_CFLAGS_COMPILE =
    let gstPluginPaths =
          lib.makeSearchPathOutput "lib" "/lib/gstreamer-1.0" gstDeps;
    in [
      "-I${gst_all_1.gstreamer.dev}/lib/gstreamer-1.0/include"
      ''-DGST_PLUGIN_PATH_1_0="${gstPluginPaths}"''
    ];

  buildInputs =  gstDeps ++ xorgDeps ++ commonDeps;

  nativeBuildInputs = [ pkgconfig cmake ];

  cmakeFlags = [
    "-DENABLE_GPL=ON"
    "-DCMAKE_BUILD_TYPE=${if debug then "Debug" else "Release"}"
  ];
}
