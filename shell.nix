with (import <nixpkgs> {});

let
  pySatie = import ./pySatie.nix;
  libShmdata = import ./libShmdata.nix;
in mkShell {
  buildInputs = [
    supercollider_scel
    libShmdata
    pySatie
  ];
}
